﻿using System;
using Application.DataAccess.Postgres.EntityConfigurations;
using Application.Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Postgres
{
    public class DataContext: DbContext
    {
        public DbSet<Request> Requests { get; set; }
        public DbSet<Bet> Bets { get; set; }

        public DataContext()
        {

        }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new RequestConfiguration());
            modelBuilder.ApplyConfiguration(new BetConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
