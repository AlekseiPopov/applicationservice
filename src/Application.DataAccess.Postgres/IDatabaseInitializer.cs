﻿using DataAccess.Postgres;

namespace Application.DataAccess.Postgres
{
    public interface IDatabaseInitializer
    {
        public void Initialize(DataContext dbContext);

        public void Initialize();
    }
}