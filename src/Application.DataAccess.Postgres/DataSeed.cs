﻿using System;
using System.Collections.Generic;
using Application.Entities.Models;

namespace Application.DataAccess.Postgres
{
    public class DataSeed
    {
        public static IEnumerable<Request> Requests => new List<Request>()
        {
            new Request()
            {
                Id = Guid.Parse("1c7dab91-3317-41a5-adf5-8682fa3d12af"),
                StudentId = Guid.Parse("4122d412-682a-4bad-9837-ceda4824e261"),
                SubjectId = Guid.Parse("991533d5-d8d5-4a14-9c7b-eb9f14e1a333"),
                TeacherId = Guid.Parse("2c73101f-d2d4-46e4-9e29-93459dc71bc7"),
                DesiredDateTime = DateTime.Now,
                Description = "I want to study English",
                ActuallUntil = DateTime.Now.AddDays(3),
                CreationDate = DateTime.Now.AddDays(1),
                Status = Entities.Enums.RequestStatus.Active,
                Price = 1900.50F
            },
            new Request()
            {
                Id = Guid.Parse("b296fb4f-e1fa-401b-907f-f28a88f47cf6"),
                StudentId = Guid.Parse("21092523-4d24-4909-b774-751de633ed95"),
                SubjectId = Guid.Parse("991533d5-d8d5-4a14-9c7b-eb9f14e1a111"),
                TeacherId = Guid.Parse("0d3acdc7-ee94-4e22-8edc-aaaf92072eac"),
                DesiredDateTime = DateTime.Now,
                Description = "I want to study math",
                ActuallUntil = DateTime.Now.AddDays(6),
                CreationDate = DateTime.Now.AddDays(3),
                Status = Entities.Enums.RequestStatus.Active,
                Price = 300.2F
            },
            new Request()
            {
                Id = Guid.Parse("2ec1dcdc-b2b5-4a9e-8ace-33a8ae240f0f"),
                StudentId = Guid.Parse("21092523-4d24-4909-b774-751de633ed95"),
                SubjectId = Guid.Parse("991533d5-d8d5-4a14-9c7b-eb9f14e1a111"),
                DesiredDateTime = DateTime.Now,
                Description = "I want to study qeometry",
                ActuallUntil = DateTime.Now.AddDays(6),
                CreationDate = DateTime.Now.AddDays(3),
                Status = Entities.Enums.RequestStatus.Active,
                Price = 300.2F
            },
            new Request()
            {
                Id = Guid.Parse("1989e6d9-997d-445a-8233-3f57ac61765b"),
                StudentId = Guid.Parse("21092523-4d24-4909-b774-751de633ed95"),
                SubjectId = Guid.Parse("991533d5-d8d5-4a14-9c7b-eb9f14e1a111"),
                DesiredDateTime = DateTime.Now,
                Description = "I want to study biology",
                ActuallUntil = DateTime.Now.AddDays(6),
                CreationDate = DateTime.Now.AddDays(3),
                Status = Entities.Enums.RequestStatus.Active,
                Price = 300.2F
            }
        };

        public static IEnumerable<Bet> Bets = new List<Bet>()
        {
            new Bet()
            {
                Id = Guid.Parse("42028852-42bb-4f97-be98-f8b89a02b002"),
                RequestId = Guid.Parse("b296fb4f-e1fa-401b-907f-f28a88f47cf6"),
                TeacherId = Guid.Parse("2c73101f-d2d4-46e4-9e29-93459dc71bc7"),
                Price = 654.89F,
                Description = "Готов поработать за такую сумму",
                CreationDate = DateTime.Now
            },
            new Bet()
            {
                Id = Guid.Parse("5c7176a5-1ec2-487a-85e7-b8c60f3b0490"),
                RequestId = Guid.Parse("1c7dab91-3317-41a5-adf5-8682fa3d12af"),
                TeacherId = Guid.Parse("0d3acdc7-ee94-4e22-8edc-aaaf92072eac"),
                Price = 1200F,
                Description = "Готов поработать за такую сумму",
                CreationDate = DateTime.Now
            }
        };
    }
}
