﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Entities.Models;

namespace Application.Infrastructure.Interfaces
{
    public interface IRequestRepository : IBaseRepository<Request>
    {
        Task<Request> GetRequestForUpdateAsync(Guid id);
        Task<ICollection<Request>> GetRequestsForStudent(Guid Id);
        Task<Request> GetDetailsRequestAsync(Guid id);
        Task<IEnumerable<Request>> GetOpenedAuctions();
    }
}
