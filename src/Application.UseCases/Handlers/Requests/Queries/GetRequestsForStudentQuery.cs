﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Entities.Models;
using Application.Infrastructure.Interfaces;
using MediatR;

namespace Application.UseCases.Handlers.Requests.Queries
{
    public class GetRequestsForStudentQuery : IRequest<ICollection<Request>>
    {
        public Guid StudentId { get; set; }

        public class GetRequestsForStudentQueryHandler : IRequestHandler<GetRequestsForStudentQuery, ICollection<Request>>
        {
            private readonly IRequestRepository _requestRepository;

            public GetRequestsForStudentQueryHandler(IRequestRepository requestRepository)
            {
                _requestRepository = requestRepository;
            }
            public async Task<ICollection<Request>> Handle(GetRequestsForStudentQuery query, CancellationToken cancellationToken)
            {
                var requests = await _requestRepository.GetRequestsForStudent(query.StudentId);
                if (requests == null) return null;
                return requests;
            }
        }
    }
}
