﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Entities.Models;
using Application.Infrastructure.Interfaces;
using MediatR;

namespace Application.UseCases.Handlers.Requests.Queries
{
    public class GetOpenedAuctions : IRequest<IEnumerable<Request>>
    {
        public class GetAllRequestQueryHandler : IRequestHandler<GetOpenedAuctions, IEnumerable<Request>>
        {
            private readonly IRequestRepository _requestRepository;

            public GetAllRequestQueryHandler(IRequestRepository requestRepository)
            {
                _requestRepository = requestRepository;
            }
            public async Task<IEnumerable<Request>> Handle(GetOpenedAuctions query, CancellationToken cancellationToken)
            {
                var requestList = await _requestRepository.GetOpenedAuctions();
                if (requestList == null)
                {
                    return null;
                }
                return requestList;
            }
        }
    }
}
