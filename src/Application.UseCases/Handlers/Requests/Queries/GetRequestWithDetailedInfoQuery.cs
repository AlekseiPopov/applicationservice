﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Entities.Models;
using Application.Infrastructure.Interfaces;
using MediatR;

namespace Application.UseCases.Handlers.Requests.Queries
{
    public class GetRequestWithDetailedInfoQuery : IRequest<Request>
    {
        public Guid RequestId { get; set; }

        public class GetRequestWithDetailedInfoQueryHandler : IRequestHandler<GetRequestWithDetailedInfoQuery, Request>
        {
            private readonly IRequestRepository _requestRepository;

            public GetRequestWithDetailedInfoQueryHandler(IRequestRepository requestRepository)
            {
                _requestRepository = requestRepository;
            }

            public async Task<Request> Handle(GetRequestWithDetailedInfoQuery query, CancellationToken cancellationToken)
            {
                var Request = await _requestRepository.GetDetailsRequestAsync(query.RequestId);
                if (Request == null) return null;
                return Request;
            }
        }
    }
}
