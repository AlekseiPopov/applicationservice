﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Entities.Models;
using Application.Infrastructure.Interfaces;
using MediatR;

namespace Application.UseCases.Handlers.Requests.Queries
{
    public class GetMyRequestsQuery : IRequest<IEnumerable<Request>>
    {
        public Guid StudentId { get; set; }

        public class GetMyRequestsQueryHandler : IRequestHandler<GetMyRequestsQuery, IEnumerable<Request>>
        {
            private readonly IRequestRepository _requestRepository;

            public GetMyRequestsQueryHandler(IRequestRepository requestRepository)
            {
                _requestRepository = requestRepository;
            }
            public async Task<IEnumerable<Request>> Handle(GetMyRequestsQuery query, CancellationToken cancellationToken)
            {
                Guid loggedUserId = new Guid();

                //TODO: получить ID залогиненного пользователя у микросервиса Пользователей
                //и проверить его роль, т.е. является ли он студентом

                if (loggedUserId == query.StudentId)
                {
                    var requests = await _requestRepository.GetRequestsForStudent(query.StudentId);
                    if (requests == null) return null;
                    return requests;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
