﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Entities.Models;
using Application.Infrastructure.Interfaces;
using MediatR;

namespace Application.UseCases.Handlers.Requests.Queries
{
    public class GetRequestByIdQuery : IRequest<Request>
    {
        public Guid Id { get; set; }

        public class GetRequestByIdQueryHandler : IRequestHandler<GetRequestByIdQuery, Request>
        {
            private readonly IRequestRepository _requestRepository;
            public GetRequestByIdQueryHandler(IRequestRepository requestRepository)
            {
                _requestRepository = requestRepository;
            }
            public async Task<Request> Handle(GetRequestByIdQuery query, CancellationToken cancellationToken)
            {
                var Request = await _requestRepository.GetByIdAsync(query.Id);
                if (Request == null) return null;
                return Request;
            }
        }
    }
}