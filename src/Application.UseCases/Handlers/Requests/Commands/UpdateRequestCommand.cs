﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Entities.Enums;
using Application.Infrastructure.Interfaces;
using MediatR;

namespace Application.UseCases.Handlers.Requests.Commands
{
    public class UpdateRequestCommand : IRequest<Guid>
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public string Subject { get; set; }
        public Guid SubjectId { get; set; }
        public Guid StudentId { get; set; }
        public Guid TeacherId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ActuallUntil { get; set; }
        public DateTime DesiredDateTime { get; set; }
        public RequestStatus Status { get; set; }

        public class UpdateRequestCommandHandler : IRequestHandler<UpdateRequestCommand, Guid>
        {
            private readonly IRequestRepository _requestRepository;

            public UpdateRequestCommandHandler(IRequestRepository requestRepository)
            {
                _requestRepository = requestRepository;
            }
            public async Task<Guid> Handle(UpdateRequestCommand command, CancellationToken cancellationToken)
            {
                var request = await _requestRepository.GetRequestForUpdateAsync(command.Id);
                if (request == null)
                {
                    return default;
                }
                else
                {
                    request.CreationDate = DateTime.Now;
                    request.Description = command.Description;
                    request.DesiredDateTime = command.DesiredDateTime;
                    request.SubjectId = command.SubjectId;
                    request.TeacherId = command.TeacherId;
                    request.StudentId = command.StudentId;
                    request.Subject = command.Subject;
                    request.ActuallUntil = request.DesiredDateTime.AddMonths(1);
                    request.Status = command.Status;

                    await _requestRepository.UpdateAsync(request);
                    return request.Id;
                }
            }
        }
    }
}
