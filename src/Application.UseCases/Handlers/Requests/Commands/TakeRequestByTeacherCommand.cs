﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Entities.Enums;
using Application.Entities.Models;
using Application.Infrastructure.Interfaces;
using MediatR;

namespace Application.UseCases.Handlers.Requests.Commands
{
    public class TakeRequestByTeacherCommand : IRequest<Guid>
    {
        public Guid RequestId { get; set; }
        public Guid TeacherId { get; set; }

        public class TakeRequestByTeacherCommandHandler : IRequestHandler<TakeRequestByTeacherCommand, Guid>
        {
            private readonly IRequestRepository _requestRepository;

            public TakeRequestByTeacherCommandHandler(IRequestRepository requestRepository)
            {
                _requestRepository = requestRepository;
            }

            public async Task<Guid> Handle(TakeRequestByTeacherCommand command, CancellationToken cancellationToken)
            {
                var request = await _requestRepository.GetRequestForUpdateAsync(command.RequestId);

                //TODO : Проверить TeacherId является ли он преподавателем у микросервиса Ползователей

                if (request == null)
                {
                    return default;
                }
                else
                {
                    request.TeacherId = command.TeacherId;
                    request.Status = RequestStatus.Closed;

                    await _requestRepository.UpdateAsync(request);

                    //TODO: Отправить запрос на создание урока в микросервис Уроков

                    return request.Id;
                }
            }
        }
    }
}