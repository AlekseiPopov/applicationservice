﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Entities.Enums;
using Application.Entities.Models;
using Application.Infrastructure.Interfaces;
using MediatR;

namespace Application.UseCases.Handlers.Requests.Commands
{
    public class CancelRequestCommand : IRequest<Guid>
    {
        public Guid RequestId { get; set; }
        public Guid StudentId { get; set; }

        public class CancelRequestCommandHandler : IRequestHandler<CancelRequestCommand, Guid>
        {
            private readonly IRequestRepository _requestRepository;

            public CancelRequestCommandHandler(IRequestRepository requestRepository)
            {
                _requestRepository = requestRepository;
            }

            public async Task<Guid> Handle(CancelRequestCommand command, CancellationToken cancellationToken)
            {
                var request = await _requestRepository.GetRequestForUpdateAsync(command.RequestId);

                //TODO : Проверить является ли StudentId студентом у микросервиса Ползователей

                if (request == null)
                {
                    return default;
                }
                else
                {
                    request.Status = RequestStatus.Canceled;

                    await _requestRepository.UpdateAsync(request);

                    return request.Id;
                }
            }
        }
    }
}