﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Infrastructure.Interfaces;
using MediatR;

namespace Application.UseCases.Handlers.Requests.Commands
{
    public class DeleteRequestByIdCommand : IRequest<Guid>
    {
        public Guid Id { get; set; }

        public class DeleteRequestByIdCommandHandler : IRequestHandler<DeleteRequestByIdCommand, Guid>
        {
            private readonly IRequestRepository _requestRepository;

            public DeleteRequestByIdCommandHandler(IRequestRepository requestRepository)
            {
                _requestRepository = requestRepository;
            }
            public async Task<Guid> Handle(DeleteRequestByIdCommand command, CancellationToken cancellationToken)
            {
                var request = await _requestRepository.GetByIdAsync(command.Id);

                if (request == null) return default;
                await _requestRepository.DeleteAsync(request);

                return request.Id;
            }
        }
    }
}
