﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading;
using System.Threading.Tasks;
using Application.Entities.Enums;
using Application.Entities.Models;
using Application.Infrastructure.Interfaces;
using Application.Infrastructure.Interfaces.OutsideModels;
using Application.Infrastructure.Interfaces.Services;
using MediatR;
using RestSharp;

namespace Application.UseCases.Handlers.Requests.Commands
{
    public class CreateRequestCommand : IRequest<Guid>
    {
        public string Description { get; set; }
        public string Subject { get; set; }
        public Guid SubjectId { get; set; }
        public Guid StudentId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ActuallUntil { get; set; }
        public DateTime DesiredDateTime { get; set; }
        public RequestStatus Status { get; set; }

        public class CreateRequestCommandHandler : IRequestHandler<CreateRequestCommand, Guid>
        {
            private readonly IRequestRepository _requestRepository;

            private readonly INotificationService _notificationService;

            public CreateRequestCommandHandler(IRequestRepository requestRepository, INotificationService notificationService)
            {
                _requestRepository = requestRepository;
                _notificationService = notificationService;
            }

            public async Task<Guid> Handle(CreateRequestCommand command, CancellationToken cancellationToken)
            {
                var Request = new Request
                {
                    CreationDate = DateTime.Now,
                    Description = command.Description,
                    DesiredDateTime = command.DesiredDateTime,
                    SubjectId = command.SubjectId,
                    StudentId = command.StudentId,
                    Subject = command.Subject
                };

                Request.ActuallUntil = Request.DesiredDateTime.AddMonths(1);
                Request.Status = command.Status;
                Guid requestId = await _requestRepository.CreateAsync(Request);


                var request = new RestRequest("api/Email", Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.AddJsonBody(new Email
                {
                    Address = "Lexs200610@yandex.ru",
                    Subject = "Новая заявка",
                    Body = "Поздравляем Вы успешно создали заявку на портале Jumping"
                });

                _notificationService.Execute(request);

                return requestId;
            }
        }
    }
}
