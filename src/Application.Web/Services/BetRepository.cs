﻿using System;
using Microsoft.EntityFrameworkCore;
using Application.Entities.Models;
using Application.Infrastructure.Interfaces;
using DataAccess.Postgres;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace Application.Web.Services
{
    public class BetRepository : BaseRepository<Bet>, IBetRepository
    {
        public BetRepository(DataContext context) : base(context)
        {

        }
    }
}
