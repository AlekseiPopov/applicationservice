﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Entities.Enums;

namespace Application.Entities.Models
{
    public class Request : BaseEntity
    {
        public Request()
        {
            Bets = new List<Bet>();
        }

        public string Description { get; set; }
        public string Subject { get; set; }
        public Guid SubjectId { get; set; }
        public Guid StudentId { get; set; }
        public Guid? TeacherId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ActuallUntil { get; set; }
        public DateTime DesiredDateTime { get; set; }
        public RequestStatus Status { get; set; }
        public float Price { get; set; }
        public int BetsCount { get { return Bets.Count; } }

        public ICollection<Bet> Bets { get; set; }
    }
}

