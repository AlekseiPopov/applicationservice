﻿using System;
namespace Application.Entities.Enums
{
    public enum BetStatus
    {
        Active,
        Accepted,
        Canceled
    }
}
