﻿namespace Application.Entities.Enums
{
    public enum RequestStatus
    {
        Active,
        Closed,
        Canceled
    }
}
